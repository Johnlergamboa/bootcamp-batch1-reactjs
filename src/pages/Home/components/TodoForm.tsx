import { ITodoFormProp } from "../type";

function TodoForm(props: ITodoFormProp) {
    const { inputHandler, messageHandler, btnAddHandler, temp_input, temp_message } = props;
  return (
    <div>
      <input
        name="title"
        type="text"
        onChange={inputHandler}
        value={temp_input}
      />
      <textarea
        name="message"
        onChange={messageHandler}
        value={temp_message}
      ></textarea>
      <button className="btn" onClick={btnAddHandler}>
        Add
      </button>
    </div>
  );
}

export default TodoForm;
